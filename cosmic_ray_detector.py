import time

sensor = int(1)

def cosmic_ray_detector():
    """ Using a int/long to detect bits flipped
        by cosmic rays or anything else out there.
    """
    global sensor

    while not ((sensor > 1) or (sensor < 1)):
        time.sleep(0.1)

    print("You've caught a cosmic ray")
    sensor = int(1)

    return cosmic_ray_detector()

if __name__ == "__main__":
    cosmic_ray_detector()
